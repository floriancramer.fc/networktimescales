import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import re
from scipy.stats import linregress

from utils.file_reader import extract_params, get_file_content, get_file_contents
from utils.plotting import simple_scatter, simple_scatter_multiple, regression_plot, regression_plot_multiple_shaded
from correlation_analysis_single_trial import run_analysis as get_decay_constant
# from utils.processing import get_decay_constant

def find_file_with_string(dir_path, search_string):
    # The filenames include the parameters which were used to generate them, 
    # so this function extracts the parameter value from the filename
    
    # Check if the directory exists
    if not os.path.exists(dir_path):
        print(dir_path)
        print("Directory does not exist.")
        return None
    
    # Get list of files in the directory
    files = os.listdir(dir_path)
    
    for file in files:
        # Check if the file name contains the search string
        if search_string in file:
            return dir_path + file

def calc_network_frequency(t_ref=2.0*10**(-3), theta=15*10**(-3),J=0.2*10**(-3), g=6.0, gamma = 0.25, V_reset = 10*10**(-3), C_E = 100):
    a = 1.0/t_ref
    b = (theta-V_reset)
    c = (1.0 - g * gamma)
    d = C_E * J * c
    e = 1.0/d
    print(a)
    print(b)
    print(e)
    return a * b * e
    pass

def calc_threshold_frequency(V_th = 15*10**(-3), C_E = 100, J = 0.2*10**(-3), tau=0.1*10**(-3)):
    return V_th / (C_E * J * tau)

def get_mean_firing_rate(dir):
    ex_spikes = get_file_contents(dir + 'ex_spikes/')

    # Sample data (replace with your actual data)
    spike_times = [time for (time, sender) in ex_spikes[0][:]]
    neuron_ids = [sender for (time, sender) in ex_spikes[0][:]]

    
    spike_counts = []
    for i in range(len(list(set(neuron_ids)))):
        spike_counts.append(len([sender for sender in neuron_ids if sender==i]))
    mean_spike_count = np.mean(spike_counts)
    
    # spike recorder runs for 100ms = 0.1s
    mean_spike_rate = mean_spike_count/ 0.1
    return mean_spike_rate

def run_analysis(included_trials, show_plots=False):
    t_refs = ['1.0','1.5', '2.0', '2.5', '3.0', '3.5', '4.0']
    delays = ['0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5','4.0']
    gs = ['1.0', '1.5', '2.0', '2.5', '3.0','3.5', '4.0','4.5','5.0','5.5','6.0', '6.5', '7.0','7.5','8.0']
    max_values = ['1.0','2.0','3.0','4.0','5.0','6.0','7.0','8.0']
    tau_ms = ['12.5','15.0','17.5','20.0','22.5','25.0','27.5']
    Js = ['0.15','0.2','0.25','0.3','0.35']

    trials = []
    for x in included_trials:
        if 't_ref' in x:
            for y in t_refs:
                trials.append(f'{x}{y}/')
        elif 'delay' in x:
            for y in delays:
                trials.append(f'{x}{y}/')
        elif 'g/' in x:
            for y in gs:
                trials.append(f'{x}{y}/')
        elif 'max_value' in x:
            for y in max_values:
                trials.append(f'{x}{y}/')
        elif 'J' in x:
            for y in Js:
                trials.append(f'{x}{y}/')
        elif 'tau_m' in x:
            for y in tau_ms:
                trials.append(f'{x}{y}/')

    # get the corresponding capacities
    capacity_file_paths = [find_file_with_string(dir + trial, step_duration) for trial in trials]
    capacity_file_paths = [x for x in capacity_file_paths if x is not None]

    contents = [get_file_content(file) for file in capacity_file_paths]

    # calculate metrics
    total_capacities = [np.sum([item.get('score') for item in content])  if len(content)>0 else 0 for content in contents]
    linear_capacities = [np.sum([item.get('score') if item.get('degree')==1 else 0 for item in content]) if len(content)>0 else 0 for content in contents]
    max_delays = [np.max([item.get('delay') for item in content])  if len(content)>0 else 0 for content in contents]
    max_degrees = [np.max([item.get('degree') for item in content])  if len(content)>0 else 0 for content in contents]


    # decide for which timescale to measure against
    timescale = []

    if timescale_name=='network_frequency':
        timescale = [get_mean_firing_rate(dir + trial) for trial in trials]

    if timescale_name=='decay_constant':
        timescale = [ get_decay_constant(dir + trial) for trial in trials]

    metric = []

    if metric_name=='total_capacity':
        metric = total_capacities
    elif metric_name=='linear_capacity':
        metric = linear_capacities
    elif metric_name=='max_delays':
        metric = max_delays
    elif metric_name=='max_degrees':
        metric = max_degrees 

    cmap = plt.cm.get_cmap('tab10')
    # if len(metric%2==0):
    colorsA = cm.Blues(np.linspace(0.4, 1.0, len(metric)//2))
    colorsB = cm.Oranges(np.linspace(0.4, 1.0, len(metric)//2))
    colors = np.concatenate((colorsA, colorsB))
    if len(colors)!=len(timescale):
        colors = cm.Blues(np.linspace(0.4, 1.0, len(metric)))
    if show_plots:
        simple_scatter(timescale, metric, title=f'Capacity based on Timescale for multiple Models ', xlabel=f'autocorrelation decay constant in ms', ylabel=f'capacity', colors=colors)

    print(f'x: {len(timescale)}, y: {len(metric)}, colors: {len(colors)}')
    return timescale, metric, colors


metric_name = 'total_capacity'

timescale_name = 'decay_constant'

step_duration = 'stepduration=25.0'

runs = [
   [
        'modelA/parameter_variation/t_ref/t_ref=',
        'modelB/parameter_variation/t_ref/t_ref=',
   ],
    [
        'modelA/parameter_variation/delay/delay=',
        'modelB/parameter_variation/delay/delay=',
    ],
    [
        'modelA/parameter_variation/tau_m/tau_m=',
        'modelB/parameter_variation/tau_m/tau_m=',
    ],
    [
        'modelA/parameter_variation/J/J=',
        'modelB/parameter_variation/J/J=',
    ],
    [
        'modelA/parameter_variation/input_max_value/input_max_value=',
        'modelB/parameter_variation/input_max_value/input_max_value=',
    ],
    [
        'modelB/parameter_variation/g/g=',
    ] 
]

dir = 'results/trial_capacities_p=1.0/'


# show data point grouped by the corresponding parameter change
xss = []
yss = []
colors = []
for x in runs:
    xs, ys, cs = run_analysis(x)
    xss.append(xs)
    yss.append(ys)
    colors.append(cs)

print(f'total: x:{len(xss)}, y:{len(yss)}, colors: {len(colors)}')
regression_plot_multiple_shaded(
    xss,
    yss,
    titles = ['t_ref', 'delay', 'tau_m','J','input_max_value','g'],
    xlabels=[''] * 3 + ['decay constant in ms']*3,
    ylabels=['capacity']*len(xss),
    colors=colors,
    shape=(2,3)
)

def flatten(arr):
    final = []
    for a in arr:
        for x in a:
            final.append(x)
    return final
    
# show all data point

simple_scatter(flatten(xss[:]), flatten(yss[:]), 'Capacity by Decay Constant',xlabel='decay constant in ms', ylabel='capacity', colors=['blue']*len(flatten(yss[:])))

# make a linear fit through the data
x_values = np.array(flatten(xss))
# y_values = coefficients[1] + x_values*coefficients[0]
slope, intercept, r_value, p_value, std_err = linregress(flatten(xss), flatten(yss))
# y_values = intercept + x_values * slope
y_values = x_values
mse = np.mean((flatten(yss)-y_values)**2)
print(f'r_value: {r_value}, MSE: {mse}')

# show the data with a regression line
regression_plot(flatten(xss), flatten(yss), y_values,'Capacity by Decay Constant', xlabel='decay constant in ms', ylabel='capacity')