import numpy as np
import re
import matplotlib.pyplot as plt

from utils.plotting import simple_plot, plot_multiline, plot_multiline_multiple
from utils.file_reader import get_file_contents, get_file_content, extract_params

dir_A = 'results/trial_capacities_p=1.0/modelA/step_size_variation/t_ref/t_ref=2.0/'
dir_B = 'results/trial_capacities_p=1.0/modelB/step_size_variation/t_ref/t_ref=2.0/'

# extract capacity logs
params, filenames_A = extract_params(dir_A)
params, filenames_B = extract_params(dir_B)

contents_A = [np.load(dir_A + filename, allow_pickle=True) for filename in filenames_A]
contents_B = [np.load(dir_B + filename, allow_pickle=True) for filename in filenames_B]


# set up x-axis
sd_min = 2.5
sd_max = 50.0
sd_delta = 2.5
step_durations = np.arange(sd_min, sd_max + sd_delta, sd_delta)

# calculate metrics for each model
tc_A = [np.sum([item.get('score') for item in content])  if len(content)>0 else 0 for content in contents_A]
lc_A = [np.sum([item.get('score') if item.get('degree')==1 else 0 for item in content]) if len(content)>0 else 0 for content in contents_A]
mdl_A = [np.max([item.get('delay') for item in content])  if len(content)>0 else 0 for content in contents_A]
mdg_A = [np.max([item.get('degree') for item in content])  if len(content)>0 else 0 for content in contents_A]

tc_B = [np.sum([item.get('score') for item in content])  if len(content)>0 else 0 for content in contents_B]
lc_B = [np.sum([item.get('score') if item.get('degree')==1 else 0 for item in content]) if len(content)>0 else 0 for content in contents_B]
mdl_B = [np.max([item.get('delay') for item in content])  if len(content)>0 else 0 for content in contents_B]
mdg_B = [np.max([item.get('degree') for item in content])  if len(content)>0 else 0 for content in contents_B]


# logs for sanity check
print(f'step durations: {step_durations}')
print(f'total capacities:{tc_A}')
print(f'max delays: {mdl_A}')
print(f'max degrees: {mdg_A}')

plot_multiline_multiple(
    step_durations, 
    [[tc_A, lc_A], [mdg_A, mdl_A],[tc_B,lc_B],[mdg_B, mdl_B]],
    legend=[['total capacity', 'linear capacity'], ['max degree','max delay']]*2,
    titles=['Linear and Total Capacity', 'Memory-Nonlinearity Tradeoff', '', ''], 
    xlabel= ['']*2 + ['step duration in ms']*2,
    ylabel=['capacity', 'degree/delay']*2,
    shape=(2,2))
