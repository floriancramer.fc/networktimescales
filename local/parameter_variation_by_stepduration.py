import numpy as np
import re
import matplotlib.pyplot as plt

from utils.plotting import simple_plot, plot_multiline, simple_scatter, plot_multiline_multiple
from utils.file_reader import get_file_contents, get_file_content, extract_params


def trial_capacity_analysis(dir):
    # calculate the metrics for all files in a given directory
    steps, filenames = extract_params(dir)
    
    contents = [get_file_content(dir + file) for file in filenames]

    total_capacities = [np.sum([item.get('score') for item in content])  if len(content)>0 else 0 for content in contents]
    linear_capacities = [np.sum([item.get('score') if item.get('degree')==1 else 0 for item in content]) if len(content)>0 else 0 for content in contents]
    max_delays = [np.max([item.get('delay') for item in content])  if len(content)>0 else 0 for content in contents]
    max_degrees = [np.max([item.get('degree') for item in content])  if len(content)>0 else 0 for content in contents]
    
    return steps, total_capacities, linear_capacities, max_delays, max_degrees

param_name = 'delay'
tcs_A = []
mdls_A = []
mdgs_A = []
lcs_A = []
tcs_B = []
mdls_B = []
mdgs_B = []
lcs_B = []
steps = []
t_ref_values = np.arange(1.0, 4.5, 0.5)
delays = ['0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0']
tau_ms = ['12.5','15.0','17.5','20.0','22.5','25.0','27.5','30.0']

params = t_ref_values

dir_A = f'results/trial_capacities_p=1.0/modelA/step_size_variation/{param_name}/'
dir_B = f'results/trial_capacities_p=1.0/modelB/step_size_variation/{param_name}/'

for param in params:
    steps, total, linear, max_delay, max_degree = trial_capacity_analysis(dir_A + f'{param_name}={param}/')
    steps = steps[:]
    mdgs_A.append(max_degree[:])
    mdls_A.append(max_delay)
    tcs_A.append(total)
    lcs_A.append(linear)

for param in params:
    steps, total, linear, max_delay, max_degree = trial_capacity_analysis(dir_B + f'{param_name}={param}/')
    steps = steps[:]
    mdgs_B.append(max_degree[:])
    mdls_B.append(max_delay)
    tcs_B.append(total)
    lcs_B.append(linear)
    

plot_multiline_multiple(
    xs = steps,
    yss= [tcs_A, mdgs_A,mdls_A, tcs_B, mdgs_B, mdls_B],
    legend= [params,params,params]*2,
    titles=['Total Capacities','Max Degree', 'Max Delay','', '', ''],
    xlabel=['']*3 + ['step duration in ms']*3,
    ylabel=['capacity','max degree', 'max delay']*2,
    shape=(2,3),
    param_name = param_name
)

# ys = [np.mean(x) for x in tcs]
# simple_plot(params, ys, title='average total capacity based on t_ref (g=2)', xlabel='t_ref', ylabel='average total capacity')
