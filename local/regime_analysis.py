import numpy as np
import matplotlib.pylab as plt
import os
import re
from elephant.statistics import isi, cv
from elephant.spike_train_correlation import correlation_coefficient
from elephant.conversion import BinnedSpikeTrain
from neo.core import SpikeTrain
import quantities as pq

from utils.file_reader import get_file_contents
from utils.plotting import raster_plot, raster_plot_multiple

dir = 'results/trial_capacities_p=1.0/modelB/baseline/'

def get_spiketimes(dir):
    print(dir)
    # extracting data
    trial = get_file_contents( dir + 'ex_spikes/')[0]
    ex_spikes = [(time, sender) for (time, sender) in trial]
    ex_senders = [[sender for (time, sender) in trial] for trial in get_file_contents(dir + 'ex_spikes/')][0]
    total_spikes = [time for (time, sender) in trial]
    senders = list(set(ex_senders))
    spiketimes_by_sender = []
    for i in senders:
        spiketimes_by_sender.append([time for (time, sender) in ex_spikes if sender==i])
        
    cv_list = [cv(isi(spiketrain)) for spiketrain in spiketimes_by_sender]

    # calculate Pearson coefficient
    spike_trains = [SpikeTrain(timesteps*pq.ms, t_start=0 * pq.ms, t_stop=2000.0 * pq.ms) for timesteps in spiketimes_by_sender]
    binned_spike_trains = BinnedSpikeTrain(spike_trains, bin_size=5*pq.ms)
    corrcoef = correlation_coefficient(binned_spike_trains)
    cv_values = [float(x) for x in cv_list]

    print(f'mean corrcoef: {np.nanmean(corrcoef)}')
    print(f'mean cv: {np.nanmean(cv_values)}')
        
    return spiketimes_by_sender

# raster plots for noise inputs
spikes = [get_spiketimes(x) for x in ['results/trial_noise_input/V_th=20/modelA/','results/trial_noise_input/V_th=20/modelB/', 'results/trial_noise_input/V_th=20/modelC/','results/trial_noise_input/V_th=20/modelD/']]
raster_plot_multiple(spikes, titles=['Model A', 'Model B', 'Model C', 'Model D'], x_min=1000, x_max=1100, shape=(2,2))
    
# raster plots for distributed-value input
spikes = [get_spiketimes(x) for x in  ['results/trial_capacities_p=1.0/modelA/baseline/', 'results/trial_capacities_p=1.0/modelB/baseline/', 'results/trial_capacities_p=1.0/modelC/baseline/', 'results/trial_capacities_p=1.0/modelD/baseline/']]
raster_plot_multiple(spikes, titles=['Model A', 'Model B', 'Model C', 'Model D'], x_min=1000, x_max=1100, shape=(2,2))
    