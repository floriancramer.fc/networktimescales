import numpy as np
import os
import re

from utils.plotting import simple_scatter, simple_plot

def get_files_in_dir(dir):
    file_names = [file for file in os.listdir(dir) if file.endswith(".npy")]
    # print(file_names)
    return file_names


def get_file_content(file):
    # print(file)
    return np.load(file, allow_pickle=True)

def get_file_contents(dir):
    file_paths = get_files_in_dir(dir)
    contents = []
    for filepath in file_paths:
        content = np.load(dir + filepath, allow_pickle=True)
        contents.append(content)
    return contents

def get_corrEE(dir):
    # dir = 'results/correlation/'
    contents = get_file_contents(dir)
    return [content.item().get('count_covariance')[0][0] for content in contents]

def get_corrEI(dir):
    # dir = 'results/correlation/'
    contents = get_file_contents(dir)
    return [content.item().get('count_covariance')[0][1] for content in contents]

def get_corrIE(dir):
    # dir = 'results/correlation/'
    contents = get_file_contents(dir)
    return [content.item().get('count_covariance')[1][0] for content in contents]


def get_corrII(dir):
    # dir = 'results/correlation/'
    contents = get_file_contents(dir)
    return [content.item().get('count_covariance')[1][1] for content in contents]


def get_corrmat(dir):
    contents = get_file_contents(dir)
    return [content.item().get('count_covariance') for content in contents]


def extract_params(directory, param_name='stepduration'):
    param_values = []
    filenames = []

    # Iterate over files in the directory
    for filename in os.listdir(directory):
        filepath = os.path.join(directory, filename)
        
        # Check if the path is a file and has .npy extension
        if os.path.isfile(filepath) and filename.endswith('.npy'):
            # Extract parameter value from the filename using regular expressions
            match = re.search(r'stepduration=([\d.]+)', filename)
            if match:
                param_value = float(match.group(1))  # Extract parameter value as float
                param_values.append(param_value)
                filenames.append(filename)

    # Sort filenames based on parameter values
    param_values, filenames = zip(*sorted(zip(param_values, filenames)))

    return list(param_values), list(filenames)

# dir = 'results/simplebrunel/'


# average_EEs, std_EEs = average_series(get_corrEE(dir))

# times = np.arange(0, 200.5, 0.5)

# print(len(average_EEs))

# simple_plot(times, average_EEs, 'Crosscorrelation Exitatory-Inhibitory Neurons', 'time in ms', 'counts')

# content = np.load('test_input_mixed.npy', allow_pickle=True)

# print(content.shape)


