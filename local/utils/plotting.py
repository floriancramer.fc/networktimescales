import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

def configure_axis(ax, x, y, title='', xlabel='', ylabel=''):    
    # Set the title, xlabel, and ylabel
    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)   
     
    return ax

def simple_plot(x, y, title='', xlabel='', ylabel=''):
    fig , ax = plt.subplots()
    ax.plot(x,y)
    ax = configure_axis(ax, x, y, title, xlabel, ylabel)
    plt.show()

def multiple_plot(x, ys, titles, xlabel='', ylabel='',shape = (2,2)):
    if shape[0] + shape[1] != len(ys):
        print('Shape doenst fit')
        return
    fig, ax = plt.subplots(shape[0], shape[1])
    for i,y in enumerate(ys):
        ax[i%2, i//2].plot(x, y, color='blue')
        ax[i%2, i//2] = configure_axis(ax[i%2, i//2], x, y, titles[i], xlabel=xlabel, ylabel=ylabel)
        
    plt.show()

def simple_scatter(x, y, title='', xlabel='', ylabel='', colors = [], annotations = []):
    fig , ax = plt.subplots()
    if len(colors)==len(y):
        for (a, b) in annotations:
            ax.annotate(a, b)
        ax.scatter(x,y, c=colors)
    else:
        ax.scatter(x,y, colors=['blue']*len(y))
    ax = configure_axis(ax, x, y, title, xlabel, ylabel)
    plt.subplots_adjust(hspace=0.3, left=0.04, right=0.96,bottom=0.08)
    plt.show()

def simple_scatter_multiple(xss, yss, titles = [], xlabels=[], ylabels=[], colors = [], shape=(2,2)):
    fig, ax = plt.subplots(shape[0], shape[1])
    if shape[0]==1 or shape[1]==1:
        for i, ys in enumerate(yss):
            ax[i].scatter(xss[i], ys, c=colors[i])
            ax[i] = configure_axis(ax[i], xss[i], ys[0], titles[i], xlabels[i], ylabels[i])
    else:
        for i, ys in enumerate(yss):
            ax[i//shape[1], i%shape[1]].scatter(xss[i], ys, c=colors[i])
            ax[i//shape[1], i%shape[1]] = configure_axis(ax[i//shape[1], i%shape[1]], xss, ys[0], titles[i], xlabels[i], ylabels[i])
    plt.subplots_adjust(hspace=0.3, left=0.04, right=0.96,bottom=0.08)
    plt.show()
    
def regression_plot(x, y, predicted, title='', xlabel='', ylabel=''):
    fig , ax = plt.subplots()
    ax.scatter(x,y)
    ax.plot(x,predicted, color='red')
    ax = configure_axis(ax, x, y, title, xlabel, ylabel)
    plt.subplots_adjust(hspace=0.3, left=0.04, right=0.96,bottom=0.08)
    plt.show()
    
def regression_plot_multiple(x, ys, predicted, titles, xlabel=[], ylabel=[], shape=(2,2)):
    fig, ax = plt.subplots(shape[0], shape[1])
    if shape[0]==1 or shape[1]==1:
        for i, y in enumerate(ys):
            ax[i].plot(x, y, color='blue')
            ax[i].plot(x, predicted[i], color='red')
            ax[i] = configure_axis(ax[i], x, ys[0], titles[i], xlabel[i], ylabel[i])
    else:
        for i, y in enumerate(ys):
            ax[i//shape[1], i%shape[1]].plot(x, y, color='blue')
            ax[i//shape[1], i%shape[1]].plot(x, predicted[i], color='red')
            ax[i//shape[1], i%shape[1]] = configure_axis(ax[i//shape[1], i%shape[1]], x, ys[0], titles[i], xlabel[i], ylabel[i])
    
        plt.subplots_adjust(hspace=0.3, left=0.04, right=0.96,bottom=0.08)
    plt.show()
    
def regression_plot_multiple_shaded(xss, yss, titles = [], xlabels=[], ylabels=[], colors = [], shape=(2,2)):
    fig, ax = plt.subplots(shape[0], shape[1])
    if shape[0]==1 or shape[1]==1:
        for i, ys in enumerate(yss):
            ax[i].scatter(xss[i], ys, c=colors[i])
            ax[i].plot(xss[i],xss[i], color='red')
            ax[i] = configure_axis(ax[i], xss[i], ys[0], titles[i], xlabels[i], ylabels[i])
    else:
        for i, ys in enumerate(yss):
            ax[i//shape[1], i%shape[1]].scatter(xss[i], ys, c=colors[i])
            ax[i//shape[1], i%shape[1]].plot(xss[i],xss[i], color='red')
            ax[i//shape[1], i%shape[1]] = configure_axis(ax[i//shape[1], i%shape[1]], xss, ys[0], titles[i], xlabels[i], ylabels[i])
    plt.subplots_adjust(hspace=0.3, left=0.04, right=0.96,bottom=0.08)
    plt.show()

    

def raster_plot(times, neurons, n=200, x_min=0, x_max=1000, title=''):

    sampled_neurons =  np.random.choice(len(neurons), n, replace=False)
    sampled_neurons.sort()
    
    data = []
    
    for i in range(n):
        neuron_activity = []
        for j, time in enumerate(times):
            if neurons[j]==i:
                neuron_activity.append(time)
        data.append(neuron_activity)
    
    plt.eventplot(positions=data, colors='red', orientation='horizontal', linelengths=4.0, lineoffsets=range(n))
    plt.title(title)
    plt.xlim(x_min, x_max)
    plt.xlabel("Time in ms")
    plt.ylabel("Neuron ID")
    plt.subplots_adjust(hspace=0.3, left=0.04, right=0.96,bottom=0.08)
    plt.show()

def raster_plot_multiple(spike_times_array, titles, n=200, x_min=0, x_max=100, shape= (2,2), xlabels=['','time in ms','','time in ms'], ylabel='Neuron id'):
    fig, ax = plt.subplots(shape[0], shape[1])
    plt.subplots_adjust(hspace=0.4) #wspace=0.4
    # print(f'pos: {len(spike_times_array)}, n: {n}')
    for i,y in enumerate(spike_times_array):
        ax[i%2, i//2].eventplot(positions=y[:n], colors='red', orientation='horizontal', linelengths=4.0, lineoffsets=range(n))
        ax[i%2, i//2].set_xlim(x_min,x_max)
        ax[i%2, i//2].set_ylim(0,n)
        ax[i%2, i//2] = configure_axis(ax[i%2, i//2], [], y, titles[i], xlabel=xlabels[i], ylabel=ylabel)
    plt.subplots_adjust(hspace=0.3, left=0.04, right=0.96,bottom=0.08)
    plt.show()
    
def plot_multiline(xs, ys, legend,title='', xlabel='', ylabel=''):
    fig , ax = plt.subplots()
    cmap = plt.cm.get_cmap('tab10')
    colors = cm.Blues(np.linspace(0.5, 1, len(ys)))
    for i, (y,name) in enumerate(zip(ys, legend)):
        ax.plot(xs,y, label=name, color=colors[i])
        
        
    ax = configure_axis(ax, xs, ys[0], title, xlabel, ylabel)
    plt.legend()
    plt.show()
    
def plot_multiline_multiple(xs, yss, legend, titles=[], xlabel=[], ylabel=[], shape=(1,2), param_name='', models=['model A', 'model B']):
    fig, ax = plt.subplots(shape[0], shape[1])
    colors = cm.Blues(np.linspace(0.5, 1, len(legend[0])))
    if shape[0]==1 or shape[1]==1:
        for i, ys in enumerate(yss):
            for j, (y, name) in enumerate(zip(ys,legend[i])):
                ax[i].plot(xs,y,label=name, color=colors[j])
                ax[i] = configure_axis(ax[i], xs, ys[0], titles[i], xlabel[i], ylabel[i])
            ax[i].legend(title='t_ref')
    else:
        for i, ys in enumerate(yss):
            for j, (y, name) in enumerate(zip(ys,legend[i])):
                ax[i//shape[1], i%shape[1]].plot(xs,y,label=name, color=colors[j])
                ax[i//shape[1], i%shape[1]] = configure_axis(ax[i//shape[1], i%shape[1]], xs, ys[0], titles[i], xlabel[i], ylabel[i])
            # ax[i//shape[1], i%shape[1]].legend(title='t_ref')
        fig.text(0.04, 0.95, 'Model A', ha='left', fontsize=16)
        plt.subplots_adjust(hspace=0.3, left=0.04, right=0.96,bottom=0.08)
        fig.text(0.04, 0.45, 'Model B', ha='left', fontsize=16)
        
        plt.legend(title=param_name)
    plt.show()

# change label sizes for better visibility
plt.rcParams.update({'font.size': 16})