import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from utils.file_reader import get_file_contents


def exp_fit(x,y, noise_mean=0):
    # subtract noise if necessary
    # y = y - noise_mean
    y = [x/y[0] for x in y]
    y_log = np.log(np.abs(y))
    
    model = LinearRegression()
    # fit linear to the logarithmized ys
    model.fit(x.reshape(-1,1),y_log)
    
    slope = model.coef_[0]
    intercept = model.intercept_
    return slope, np.exp(intercept)

def exp(x, a, b, c=0):
    return a * np.exp(b*x) + c

def exp_regression(x, y):
    model = LinearRegression()
    y = np.log(np.abs(y))
    model.fit(x.reshape(-1,1),y)
    slope = model.coef_[0]
    intercept = model.intercept_
    return slope, np.exp(intercept)

def ex_regression_noisy(x, y, noise):
    y = y-noise
    return exp_regression(x, y)

def average_series(data_series, axis=0):
    stacked_series = np.vstack(data_series)
    mean = np.mean(stacked_series, axis=axis)
    std = np.std(stacked_series, axis=axis)
    return mean, std

def get_neuron_power_spectrum(activity):
    dft = np.fft.fft(activity)
    power_spectrum = np.abs(dft)**2 / len(activity)
    return power_spectrum

def get_spiktrain_correlation(xs, ys, T=10000, delta = 0.5):
    
    ntrials = len(xs)
    nx_mean = np.sum(len(x) for x in xs)/ntrials
    ny_mean = np.sum(len(y) for y in ys)/ntrials
    
    rx = nx_mean/T
    ry = ny_mean/T
    
    
    print(f'ntrials:{ntrials}, rx: {rx}')
    binned_xs = [np.histogram(x, bins=np.arange(0, 1000.1, 0.1))[0] for x in xs]
    binned_ys = [np.histogram(y, bins=np.arange(0, 1000.1, 0.1))[0] for y in ys]
    
    # autocorrelations_x = [np.correlate(x-rx, x-rx, mode='full')/T for x in binned_xs]
    # autocorrelations_y = [np.correlate(y-ry, y-ry, mode='full')/T for y in binned_ys]
    crosscorrelations = [np.correlate(x-rx, y-ry, mode='full')/T for (x,y) in zip(binned_xs, binned_ys)]
    
    # discard the part storing negative delays
    # autocorrelations_x = [x[len(x)//2:] for x in autocorrelations_x]
    # autocorrelations_y = [y[len(y)//2:] for y in autocorrelations_y]
    crosscorrelations = [z[len(z)//2:] for z in crosscorrelations]
    
    # average autocorrelation over all trials
    # autocovaraince_x = [np.sum(t)/len(t) for t in np.transpose(autocorrelations_x)]
    # autocovaraince_y = [np.sum(t)/len(t) for t in np.transpose(autocorrelations_y)]
    crosscovariance = [np.sum(t)/len(t) for t in np.transpose(crosscorrelations)]
    
    # bin it into slots of time delta
    # acx = [np.sum(autocovaraince_x[5*i:5*i+5])/5 for i in range(len(autocovaraince_x)//5)]
    # acy = [np.sum(autocovaraince_y[5*i:5*i+5])/5 for i in range(len(autocovaraince_y)//5)]
    aggregation = 5
    cc = [np.sum(crosscovariance[aggregation*i:aggregation*i+aggregation])/aggregation for i in range(len(crosscovariance)//5)]
    
    return cc

def get_autocrosscorrelation(xs, ys, T=10000, delta=0.5):
    return get_spiktrain_correlation(xs,xs,T=T,delta=delta), get_spiktrain_correlation(xs,ys,T=T,delta=delta), get_spiktrain_correlation(ys,ys,T=T,delta=delta)

def get_decay_constant(dir, reg_xmax=6, x_min=1000, x_max=2000):
    ex_spike_trials = get_file_contents(dir + 'ex_spikes/')
    inh_spike_trials = get_file_contents(dir + 'inh_spikes/')
    ex_spiketimes = [[time - x_min for (time, sender) in trial] for trial in ex_spike_trials]
    inh_spiketimes = [[time-x_min for (time, sender) in trial] for trial in inh_spike_trials]
    
    times = np.arange(0, 1000.5, 0.5)
    
    acx = get_spiktrain_correlation(ex_spiketimes, ex_spiketimes)
    
    # plt.plot(acx)
    # plt.show()
    # print(acx[:50])
    
    reg_xmin = 0
    slope_x, intercept_x = exp_fit(times[reg_xmin:reg_xmax], acx[reg_xmin:reg_xmax])
    # slope_y, intercept_y = exp_fit(times[reg_xmin:reg_xmax], acy[reg_xmin:reg_xmax])
    # slope_xy, intercept_xy = exp_fit(times[reg_xmin:reg_xmax], cc[reg_xmin:reg_xmax])
    
    return -1.0/slope_x
