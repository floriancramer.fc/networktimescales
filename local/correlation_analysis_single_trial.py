import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from utils.file_reader import get_corrEE, get_corrEI, get_corrIE, get_corrII, get_file_contents
from utils.processing import exp, exp_fit
from utils.plotting import simple_scatter, simple_plot, regression_plot, regression_plot_multiple, raster_plot, multiple_plot

# correlation function provided to us
def correlation_analysis(sc,ts,pars):
        #nfft=1024
        #sampling_freq=1./pars['dt_ana']
        #noverlap=0

        ## power- and cross-spectra 
        C01_trial=np.zeros((pars['ntrials'],len(ts)-1))
        C00_trial=np.zeros((pars['ntrials'],len(ts)-1))
        C11_trial=np.zeros((pars['ntrials'],len(ts)-1))
        #C01_trial=np.zeros((pars['ntrials'],nfft/2.+1))
        #C00_trial=np.zeros((pars['ntrials'],nfft/2.+1))
        #C11_trial=np.zeros((pars['ntrials'],nfft/2.+1))
        for ct in range(pars['ntrials']):
            s0=sc[0,ct,:]
            s1=sc[1,ct,:]
            C01_trial[ct,:]=np.fft.fft(s0)*np.conjugate(np.fft.fft(s1))           ## cross-spectrum
            C00_trial[ct,:]=np.real(np.fft.fft(s0)*np.conjugate(np.fft.fft(s0)))  ## power-spectrum
            C11_trial[ct,:]=np.real(np.fft.fft(s1)*np.conjugate(np.fft.fft(s1)))  ## power-spectrum
            #C01_trial[ct,:],freqs=mlab.csd(s0,s1,NFFT=nfft,Fs=sampling_freq, window=mlab.window_hanning, noverlap=noverlap)
            #C00_trial[ct,:],freqs=mlab.psd(s0,NFFT=nfft,Fs=sampling_freq, window=mlab.window_hanning, noverlap=noverlap)
            #C11_trial[ct,:],freqs=mlab.psd(s1,NFFT=nfft,Fs=sampling_freq, window=mlab.window_hanning, noverlap=noverlap)

        ## trial averaging
        C01=np.mean(C01_trial,axis=0) / pars['T']
        C00=np.mean(C00_trial,axis=0) / pars['T']
        C11=np.mean(C11_trial,axis=0) / pars['T']
        coh=C01/np.sqrt(C00*C11)

        print("zero-frequency cross_spectrum: %.2f" % (C01[0]))
        print("zero-frequency coherence: %.2f" % (coh[0]))

        ## backtransform to time domain (auto- and cross-covariance functions)
        c01=np.fft.ifft(C01)
        c00=np.fft.ifft(C00)
        c11=np.fft.ifft(C11)

        ## normalised cross-correlation function
        cc=np.real(c01)/np.sqrt(c00[0]*c11[0])
        ac0 = np.real(c00)/np.sqrt(c00[0]*c00[0])
        ac1 = np.real(c11)/np.sqrt(c11[0]*c11[0])

        #ts=np.arange(0.,len(freqs))/freqs[-1]

        return C01,C00,C11,coh,c01,c00,c11,cc,ac0,ac1
        #return C01,C00,C11,coh,c01,c00,c11,cc,freqs,ts

def run_analysis(dir, x_shift = 0,show_plots=False):
    
    # extract spike trains
    print(get_file_contents(dir + 'ex_spikes/'))
    ex_spikestimes = [[time-x_shift for (time, sender) in trial] for trial in get_file_contents(dir + 'ex_spikes/')]
    inh_spikes = [[time-x_shift for (time, sender) in trial] for trial in get_file_contents(dir + 'inh_spikes/')]

    # binning into bins of 0.1ms width
    ex_spikes = [np.histogram(trial, np.arange(0, 10000, 0.1))[0] for trial in ex_spikestimes]
    inh_spikes = [np.histogram(trial, np.arange(0, 10000, 0.1), 1.0)[0] for trial in inh_spikes]

    ts = np.arange(0, 10000, 0.1)
    pars = {}
    pars['ntrials'] = len(ex_spikes)
    pars['T'] = 10000
    pars['K'] = 2
    sc=np.zeros((2,len(ex_spikes),len(ts)-1))

    df=1./pars['T']
    fs=df*np.arange(0,len(ts)-1)

    # sc: (chanel x trial x time)
    for i in range(len(ex_spikes)):
        sc[0][i] = ex_spikes[i]
        sc[1][i] = inh_spikes[i]


    m_trial = np.mean(sc, axis=1)
    m_time = np.mean(sc, axis=2)

    print(m_trial)

    # set type of averaging the bins
    type = 'trial'
    if show_plots:
        type = 'none'
    
    if (type == 'none'):
        print('average over trials')
        for i in range(pars['ntrials']):
            sc[0][i] = sc[0][i] - m_trial[0]
            sc[1][i] = sc[1][i] - m_trial[1]

    if (type=='time'):
        print('average over time')
        for i in range(pars['ntrials']):
            sc[0][i] = sc[0][i] - m_time[0][i]
            sc[1][i] = sc[1][i] - m_time[1][i]
        
    C01,C00,C11,coh,c01,c00,c11,cc,ac0,ac1 = correlation_analysis(sc, ts, pars)

    times = np.arange(0, 5000, 0.5)

    # convert temporal resolution from 0.1 to 0.5ms
    cc = [np.sum(cc[5*i:5*i+5])/5 for i in range(int(pars['T']/5))]
    ac0 = [np.sum(ac0[5*i:5*i+5])/5 for i in range(int(pars['T']/5))]
    ac1 = [np.sum(ac1[5*i:5*i+5])/5 for i in range(int(pars['T']/5))]

    c00 = [np.sum(c00[5*i:5*i+5])/5 for i in range(10000)]
    c01 = [np.sum(c01[5*i:5*i+5])/5 for i in range(10000)]
    c11 = [np.sum(c11[5*i:5*i+5])/5 for i in range(10000)]

    # fit exponential decay function to the correlations
    reg_min = 0
    reg_max = 5
    
    print(f'ac0: {ac0[0]}')
    print(f'cc: {cc[0]}')
    print(f'ac1: {ac1[0]}')
    slope00, intercept00 = exp_fit(times[reg_min:reg_max], ac0[reg_min:reg_max])
    slope01, intercept01 = exp_fit(times[reg_min:reg_max], cc[reg_min:reg_max])
    slope11, intercept11 = exp_fit(times[reg_min:reg_max], ac1[reg_min:reg_max])

    print('ac0: slope = ' + str(slope00) + ' intercept = ' + str(intercept00))
    print('cc: slope = ' + str(slope01) + ' intercept = ' + str(intercept01))
    print('ac1: slope = ' + str(slope11) + ' intercept = ' + str(intercept11))

    # a * exp(bx) + c
    ac0_fit = exp(times, intercept00, slope00)*ac0[0]
    cc_fit = exp(times, intercept01, slope01)*cc[0]
    ac1_fit = exp(times, intercept11, slope11)*ac1[0]
    
    # normalized correlation functions and fits
    ac0 = ac0/ac0[0]
    cc = cc/cc[0]
    ac1 = ac1/ac1[0]
    
    ac0_fit = ac0_fit/ac0_fit[0]
    cc_fit = cc_fit/cc_fit[0]
    ac1_fit = ac1_fit/ac1_fit[0]
    
    # plotting
    xmax = 50
    if show_plots:
        regression_plot_multiple(
            times[:xmax], 
            [ac0[:xmax],cc[:xmax],cc[:xmax],ac1[:xmax]], 
            [ac0_fit[:xmax], cc_fit[:xmax], cc_fit[:xmax], ac1_fit[:xmax]], 
            ['excitatory pairs', 'excitatory-inhibitory pairs', 'excitatory-inhibitory pairs', 'inhibitory pairs'],
            xlabel=['']*2 + ['time in ms']*2,
            ylabel=['']*4,
            shape=(2,2))

    # return decay constant if this script is used by plot_timescale_vs_capacity
    return (-1)/slope00

# run_analysis('results/trial_capacities_p=1.0/modelB/step_size_variation/t_ref/t_ref=2.0/',x_shift=1000, show_plots=True)
