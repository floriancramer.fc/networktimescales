import subprocess
import random
import os
import numpy as np

# Base Slurm job script
slurm_script = """#!/usr/bin/env bash
#!/bin/bash
#SBATCH -o /users/cramer/snn/logs/simulate_v{}_s{}_pid%j.out
#SBATCH -e /users/cramer/snn/logs/simulate_v{}_s{}_pid%j.err
#SBATCH --job-name imv{}_s{}
#SBATCH --mem=20000
#SBATCH --time=72:00:00
#SBATCH --ntasks 1
#SBATCH --cpus-per-task 16
#SBATCH --nodes 1
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=florian.cramer1@rwth-aachen.de

cd /users/cramer/snn/

export PYTHONPATH=$PYTHONPATH:~/snn

source activate nest

python simulate.py paramsA.yml --param_name '{}' --param_value {} --step_duration {} --seed {} --num_threads 16
"""


# Generate a random seed
seed = random.randint(1, 10000)

# number of parameter trials
n_var = 1
initial_param = 30.5
param_stepsize = 0.5

t_refs = [1.0,1.5, 2.0, 2.5, 3.0, 3.5, 4.0]
delays = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5,4.0]
gs = [1.0, 1.5, 2.0, 2.5, 3.0,3.5, 4.0,4.5,5.0,5.5,6.0, 6.5, 7.0,7.5,8.0]
max_values = [1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0]
tau_ms = [12.5,15.0,17.5,20.0,22.5,25.0,27.5]
Js = [0.15,0.2,0.25,0.3,0.35]

param_name = 'J'
param_values = Js

step_durations = [25.0, 25.0, 25.0, 25.0]
# Loop to start n jobs with different var and random seeds
for i, param_value in enumerate(param_values):
    for j, step_duration in enumerate(step_durations):
        
        job_script = slurm_script.format(f"{param_value:.1f}", f"{step_duration:.1f}", f"{param_value:.1f}", f"{step_duration:.1f}", i, j, param_name, param_value, step_duration, seed)
        
        with open(f"job_param_{i}_stepduration_{j}.sh", "w") as file:
            file.write(job_script)
        
        # Submit the job using sbatch
        subprocess.run(["sbatch", f"job_param_{i}_stepduration_{j}.sh"])
        
        # delete jobscript to prevent clutter
        os.remove(f"job_param_{i}_stepduration_{j}.sh")

        print(f"Submitted job param {param_value}  with sd {step_duration} and seed {seed}")
