import argparse
import yaml
from SNN.simulation.simulation_runner import SimulationRunner
from capacity import capacity_calculation
import numpy as np
import os

def parse_cmd():
    parser = argparse.ArgumentParser()
    parser.add_argument('parameter_file', help="path to the parameter yaml file.")
    parser.add_argument('--param_name', type=str, help='Name of the parameter to be tested')
    parser.add_argument('--param_value', type=float, help='parameter value for the current run')
    parser.add_argument("--step_duration", type=float, help="step duration value")
    parser.add_argument("--seed", type=int, help="Random seed")
    parser.add_argument('--num_threads', type=int, default=1, help="number of threads used for the simulation.")

    return parser.parse_args()


def generate_filename(params, type='standard_results', name='',seed=0):
    delay_val = params["network_params"]["delay"]
    t_ref_val = params['network_params']['neuron_params']['t_ref']
    sd_val = params['step_duration']
    g_val = params['network_params']['g']
    J_val = params['network_params']['J']
    tau_m_val = params['network_params']['neuron_params']['tau_m']
    rate_val=params['input_max_value']
    os.makedirs(f'/users/cramer/snn/results/{type}', exist_ok=True)
    return f'/users/cramer/snn/results/{type}/{name}_g={g_val}_rate={rate_val}_J={J_val}_tau_m={tau_m_val}_delay={delay_val:.1f}_t_ref={t_ref_val:.1f}_stepduration={sd_val:.1f}_seed={seed}'

if __name__ == '__main__':
    args = parse_cmd()
    with open(args.parameter_file, 'r') as param_file:
        params = yaml.safe_load(param_file)
    
    params['paramfile'] = args.parameter_file
    del params['trial']
    model_name = params['model']
    del params['model']
    
    # setting stepduration
    params['step_duration'] = args.step_duration
    params['noise_loop_duration'] = args.step_duration
    
    # setting testing parameter
    if args.param_name in ['t_ref', 'V_th', 'tau_m', 'V_reset']:
        params['network_params']['neuron_params'][args.param_name] = args.param_value
        
    elif args.param_name in ['input_max_value']:
        params[args.param_name] = args.param_value
    else:
        params['network_params'][args.param_name] = args.param_value
    
    # setting the random seed for reproducability
    np.random.seed(args.seed)
    
    # actual simulation
    simulationrunner = SimulationRunner(num_threads=args.num_threads, **params)
    results = simulationrunner.run(seed=args.seed)
    
    inputs = results[0]
    states = results[1]
    ex_spikes = results[2]
    inh_spikes = results[3]
    
    print('got out of simulation')
    
    # safe the simulation resultss
    type_name = f'{model_name}/' + f'/{args.param_name}='  + f'{args.param_value}'
    
    # print('saving inputs')
    # filename_inputs = generate_filename(params, type=type_name + '/inputs', name='inputs', seed=args.seed)
    # np.save(filename_inputs, inputs)
    
    # print('saving states')
    # filename_states = generate_filename(params=params, type=type_name + '/states', name='statemat',seed=args.seed)
    # np.save(filename_states, states)
    
    print('saving excitatory spikes')
    filename_ex_spikes = generate_filename(params, type=type_name + '/ex_spikes', name='ex_spikes', seed=args.seed)
    np.save(filename_ex_spikes, ex_spikes)
    
    print('saving inhibitory spikes')
    filename_inh_spikes = generate_filename(params, type=type_name + '/inh_spikes', name='inh_spikes', seed=args.seed)
    np.save(filename_inh_spikes, inh_spikes)
    
    # -----------------------capacity calculation-------------------------------------------------------
    cap_iter = capacity_calculation.CapacityIterator(debug=True)
    total_capacity, all_capacities, num_capacities, nodes = cap_iter.collect(inputs, states.T)
    
    print('saving capacities')
    filename_capacities = generate_filename(params=params, type=type_name, name='all_capacities',seed=args.seed)
    np.save(filename_capacities, all_capacities)
        
        
