import nest
import numpy as np

# Just a simple network for testing purposes

def add_correlation_Detector(pop_ex, pop_inh):
    # Setup correlation detector
    corr_detector = nest.Create('correlomatrix_detector')
    
    nest.SetStatus(corr_detector, {'tau_max':100.0})
    nest.SetStatus(corr_detector, {'delta_tau':0.5})
    nest.SetStatus(corr_detector, {'N_channels': 2})
    
    # Connect Correlation Detector
    nest.Connect(pop_ex, corr_detector, syn_spec={'receptor_type':0})
    nest.Connect(pop_inh, corr_detector, syn_spec={'receptor_type':1})

    return corr_detector
    
def add_spike_recorders(pop_ex, pop_inh):
    # Setup Spike recorders
    spike_recorder_ex = nest.Create('spike_recorder', params={"time_in_steps": True})
    spike_recorder_inh = nest.Create('spike_recorder', params={"time_in_steps": True})
    
    # Connect Spike Recorder
    nest.Connect(pop_ex, spike_recorder_ex)
    nest.Connect(pop_inh, spike_recorder_inh)
    
    return spike_recorder_ex, spike_recorder_inh
    
    
    

def create_brunel_network_with_noise(N=1250, p_conn=0.1, g=6.0, delay=1.5, J=0.2, sim_time=1000.0):
    """
    Create a Brunel network in NEST with separate excitatory and inhibitory populations and Poisson noise.

    Parameters:
    - N: Total number of neurons
    - p_conn: Connection probability
    - g: Ratio of inhibitory to excitatory synaptic strength
    - delay: Synaptic delay in ms
    - J: synaptic efficacy
    - sim_time: Simulation time in ms
    """
    nest.ResetKernel()
    # Enable multithreading
    nest.SetKernelStatus({"local_num_threads": 48, "resolution": 0.1, 'print_time': False})
    
    noise_rate = 15000

    # Network parameters
    NE = int(N * 0.8)  # Number of excitatory neurons
    NI = N - NE  # Number of inhibitory neurons
    
    CE = int(p_conn * NE)
    CI = int(p_conn * NI)
    
    J_ex = J
    J_in = - g * J
    
    
    # Define neuron parameters
    neuron_params = {
        "C_m": 1.0,
        "tau_m": 20.0,
        "t_ref": 2.0,
        "E_L": 0.0,
        'I_e': 0.0,
        "V_reset": 10.0,
        "V_m": 0.0,
        "V_th": 15.0,
    }

    # Create excitatory and inhibitory populations
    excitatory_neurons = nest.Create("iaf_psc_delta", NE, params=neuron_params)
    inhibitory_neurons = nest.Create("iaf_psc_delta", NI, params=neuron_params)

    # Poisson generator for background noise
    poisson_noise_ex = nest.Create("poisson_generator", params={"rate": noise_rate})
    # poisson_noise_inh = nest.Create("poisson_generator", params={"rate": 8000})
    
    # Synapse specification for background noise
    noise_ex_syn_spec = {"weight": J}
    noise_inh_syn_spec = {"weight": -g * J}


    # Synapse specifications for network connections
    syn_exc = {"weight": J_ex, "delay": delay}
    syn_inh = {"weight": J_in, "delay": delay}
    

    # ------------------------------------------------------- Connecting ---------------------------------------------------------

    # Recurrent Connections
    nest.Connect(excitatory_neurons, excitatory_neurons, {"rule": "fixed_indegree", "indegree": CE}, syn_spec=syn_exc) # EE
    nest.Connect(excitatory_neurons, inhibitory_neurons, {"rule": "fixed_indegree", "indegree": CE}, syn_spec=syn_exc) # EI
    nest.Connect(inhibitory_neurons, excitatory_neurons, {"rule": "fixed_indegree", "indegree": CI}, syn_spec=syn_inh) # IE
    nest.Connect(inhibitory_neurons, inhibitory_neurons, {"rule": "fixed_indegree", "indegree": CI}, syn_spec=syn_inh)# II
    
    
    # Connect Poisson noise to all neurons
    nest.Connect(poisson_noise_ex, excitatory_neurons,'all_to_all', syn_spec=noise_ex_syn_spec)
    nest.Connect(poisson_noise_ex, inhibitory_neurons,"all_to_all", syn_spec=noise_ex_syn_spec)
    # nest.Connect(poisson_noise_inh, excitatory_neurons,"all_to_all", syn_spec=noise_inh_syn_spec)
    # nest.Connect(poisson_noise_inh, inhibitory_neurons,"all_to_all", syn_spec=noise_inh_syn_spec)
    
    # Connect devices
    
    # Correlation detector
    corr_detector = add_correlation_Detector(excitatory_neurons, inhibitory_neurons)
        
    # Spike Recorders
    spike_recorder_ex, spike_recorder_inh = add_spike_recorders(excitatory_neurons, inhibitory_neurons)
    
    
    # --------------------------------------------------Simulation------------------------------------------------------
    # Simulation
    nest.Simulate(sim_time)
    
    
    # ----------------------------------------------Gather results-----------------------------------------------------------
    # Get Correlation Detector results
    detector_status = nest.GetStatus(corr_detector)[0]
    print(detector_status.get('count_covariance'))
    
    covmatrix = detector_status.get('count_covariance')[0]
    
    np.save('/users/cramer/snn/results/correlation/covmatrix.npy', covmatrix)
    
    
    # ex_spikes = nest.GetStatus(spike_detector_ex)[0]['events']['times']
    # inh_spikes = nest.GetStatus(spike_detector_inh)[0]['events']['times']

    ex_spikes = [(time, sender) for time, sender in zip(nest.GetStatus(spike_recorder_ex)[0]['events']['times'], nest.GetStatus(spike_recorder_ex)[0]['events']['senders'])]
    inh_spikes = [(time, sender) for time, sender in zip(nest.GetStatus(spike_recorder_inh)[0]['events']['times'], nest.GetStatus(spike_recorder_inh)[0]['events']['senders'])]
        
    
    np.save('/users/cramer/snn/results/correlation/ex_spikes.npy', ex_spikes)
    np.save('/users/cramer/snn/results/correlation/inh_spikes.npy', inh_spikes)

# Example usage
create_brunel_network_with_noise()
